export GGMSCAN_DIR=$PWD
export PATH=$GGMSCAN_DIR/scripts:$PATH
export PYTHONPATH=$GGMSCAN_DIR/lib:$GGMSCAN_DIR/scripts:$PYTHONPATH

source tools/ggm-scan-venv/bin/activate

export SPHENO_BIN=$PWD/tools/SPheno-4.0.5/bin/SPheno

FROM ubuntu:22.04

RUN echo 'APT::Install-Suggests "0";' >> /etc/apt/apt.conf.d/00-docker
RUN echo 'APT::Install-Recommends "0";' >> /etc/apt/apt.conf.d/00-docker

RUN DEBIAN_FRONTEND=noninteractive \
  apt-get update \
  && apt-get install -y python3 curl make gfortran pip python3-venv  \
  && rm -rf /var/lib/apt/lists/*

ADD . /ggmscan

ARG INSTALL_DIR=/tools
RUN mkdir ${INSTALL_DIR}
WORKDIR ${INSTALL_DIR}

RUN curl -0 "https://spheno.hepforge.org/downloads/?f=SPheno-4.0.5.tar.gz" -o SPheno-4.0.5.tar.gz && \
    tar -xvzf SPheno-4.0.5.tar.gz && \
    cd SPheno-4.0.5 && \
    sed -i 's|ifort|gfortran|g' Makefile && \
    make

# RUN curl -k -O https://wwwth.mpp.mpg.de/members/heinemey/feynhiggs/newversion/FeynHiggs-2.19.0.tar.gz && \
#     tar -xvzf FeynHiggs-2.19.0.tar.gz && \
#     cd FeynHiggs-2.19.0 && \
#     ./configure && \
#     make

RUN cd ${INSTALL_DIR} && \
    python3 -m venv --system-site-packages ggm-scan-venv && \
    . ${INSTALL_DIR}/ggm-scan-venv/bin/activate && \
    python3 -m pip install --no-cache-dir --upgrade --ignore-installed -r /ggmscan/requirements.txt


# setup file
ARG setup_file=/setup_ggmscan.sh
RUN echo "export PATH=/ggmscan/scripts:$PATH" >> ${setup_file} ; \
    echo "export PYTHONPATH=/ggmscan/lib:/ggmscan/scripts:$PYTHONPATH" >> ${setup_file} ; \
    echo "export SPHENO_BIN=${INSTALL_DIR}/SPheno-4.0.5/bin/SPheno" >> ${setup_file} ; \
    echo source ${INSTALL_DIR}/ggm-scan-venv/bin/activate >> ${setup_file}

# add it to bashrc
ARG bashrc=/etc/bash.bashrc
RUN echo source ${setup_file} >> ${bashrc}

RUN ["ln", "-sf", "/usr/bin/python3", "/usr/bin/python"]

# Create a user that does not have root privileges
ARG username=run
RUN useradd --create-home --home-dir /home/${username} ${username}
ENV HOME /home/${username}

USER ${username}
WORKDIR /home/${username}

CMD /bin/bash

# SUSYHIT utils

import os
import sys
import importlib
import subprocess
import string
import pyslha
from itertools import product
import pandas as pd

slha_template = """
Block MODSEL                 # Select model
 1    ${MODSEL}              #
Block SMINPUTS               # Standard Model inputs
 2   1.166379E-05       # G_F, Fermi constant
 3   1.184000E-01       # alpha_s(MZ) SM MSbar
 4   9.118760E+01       # Z-boson pole mass
 5   4.180000E+00       # m_b(mb) SM MSbar
 6   1.731000E+02       # m_top(pole)
 7   1.776820E+00       # m_tau(pole)
# Block SMINPUTS               # Standard Model inputs
#    1     1.27932904E+02  # alpha_em^-1(MZ)^MSbar
# #  2     1.16639000E-05  # G_mu [GeV^-2]
#    3     1.17200000E-01  # alpha_s(MZ)^MSbar
# #  4     9.11876000E+01  # m_Z(pole)
#    5     4.25000000E+00  # m_b(m_b), MSbar
#    6     1.72900000E+02  # m_t(pole)
#    7     1.77700000E+00  # m_tau(pole)
Block MINPAR                 # Input parameters
#
Block EXTPAR                 # Input parameters
   0     -1       # EWSB_scale
   1     ${M1}    # M_1
   2     ${M2}    # M_2
   3     ${M3}    # M_3
   11    ${AT}    # A_t
   12    ${AT}    # A_b
   13    ${AT}    # A_tau
   23    ${mu}    # mu(EWSB)
   25    ${tanb}  # tanbeta(MZ)
   26    ${MA}    # MA_pole
   31    ${Msq}   # M_eL
   32    ${Msq}   # M_muL
   33    ${Msq}   # M_tauL
   34    ${Msq}   # M_eR
   35    ${Msq}   # M_muR
   36    ${Msq}   # M_tauR
   41    ${Msq}   # M_q1L
   42    ${Msq}   # M_q2L
   43    ${Msq}   # M_q3L
   44    ${Msq}   # M_uR
   45    ${Msq}   # M_cR
   46    ${Msq}   # M_tR
   47    ${Msq}   # M_dR
   48    ${Msq}   # M_sR
   49    ${Msq}   # M_bR
 1000039 ${Gmass} # gravitino
Block SPhenoInput       # SPheno specific input
 1  -1                  # error level
 2   0                  # if =1, then SPA conventions are used
11   1                  # calculate branching ratios
12   1.00000000E-04     # write only branching ratios larger than this value
21   0                  # calculate cross section
22   5.00000000E+02     # cms energy in GeV
23   0.00000000E+00     # polarisation of incoming e- beam
24   0.00000000E+00     # polarisation of incoming e+ beam
25   0                  # if 0 no ISR is calculated, if 1 ISR is caculated
26   1.00000000E-04     # write only cross sections larger than this value [fb]
"""

def write_spheno_input(fname, model):

    # inputs remaining constant through the scan:
    SLHAInputTemplate = string.Template(slha_template)

    template_dict = {
        'MODSEL': 0,
    }
    template_dict.update(model)

    SLHAInput = SLHAInputTemplate.substitute(template_dict)

    with open(fname, 'w') as in_file:
        in_file.write(SLHAInput)


def read_scan_config_file(config_file):

    config_abs_path = os.path.abspath(config_file)

    import_path, config_path = config_abs_path.rsplit('/', 1)
    sys.path.append(import_path)
    print(import_path)
    print(sys.path)
    try:
        mod = importlib.import_module(config_path.replace('.py', ''))
    except:
        raise Exception('Problem in the config file... Please check!')

    scan_parameters = getattr(mod, 'scan')

    def default_filter_par_fn(pars):
        pass

    # def default_filter_slha_fn(slha_path):
    #     pass

    #global filter_fn_par_copy
    try:
       filter_fn_par = getattr(mod, 'filter_par_fn')
    except:
        print('No parameter filter function defined in config file')
        filter_fn_par = default_filter_par_fn
    # #global filter_fn_slha_copy
    # try:
    #     filter_fn_slha_copy = getattr(mod, 'filter_slha_fn')
    # except:
    #     print('No filter function defined in config file')
    #     filter_fn_slha_copy = default_filter_slha_fn

    print('-----------------------')
    print('* GGM Scan *\n')
    print('Msq = ', scan_parameters['Msq'])
    print('AT = ', scan_parameters['AT'])
    print('M1 = ', scan_parameters['M1'])
    print('M2 = ', scan_parameters['M2'])
    print('M3 = ', scan_parameters['M3'])
    print('mu = ', scan_parameters['mu'])
    print('MA = ', scan_parameters['MA'])
    print('tanb = ', scan_parameters['tanb'])
    print('Gravitino mass = ', scan_parameters['Gmass'])
    print('-----------------------\n')

    # Models
    models = prepare_list_models(scan_parameters)

    # Filter models with filter_fn_par
    for model in models[:]:
        if filter_fn_par(model):
            models.remove(model)

    return models

def prepare_list_models(scan_parameters):

    s_AT    = scan_parameters['AT']
    s_tanb  = scan_parameters['tanb']
    s_Msq   = scan_parameters['Msq']
    s_M3    = scan_parameters['M3']
    s_M2    = scan_parameters['M2']
    s_M1    = scan_parameters['M1']
    s_mu    = scan_parameters['mu']
    s_MA    = scan_parameters['MA']
    s_Gmass = scan_parameters['Gmass']

    models = []
    for AT, tanb, Msq, M3, M2, M1, mu, MA, Gmass in product(s_AT, s_tanb, s_Msq, s_M3, s_M2, s_M1, s_mu, s_MA, s_Gmass):
        models.append(
            { 'AT': AT, 'tanb': tanb, 'Msq': Msq, 'M3': M3, 'M2': M2, 'M1': M1, 'mu': mu, 'MA': MA, 'Gmass': Gmass }
        )

    return models


def create_run_directory(run_dir='.'):

    if not os.path.exists(run_dir):
        os.system('mkdir -p %s' % run_dir)


def clean_run_directory(run_dir):

    prevdir = os.getcwd()
    os.chdir(run_dir)

    # Remove Spheno tmp files
    to_remove = [
        'Messages.out',
        'input.slha',
        'output1.slha',
    ]

    for f in to_remove:
        try:
            os.remove(f)
        except OSError:
            pass

    os.chdir(prevdir)


# def add_gravitino_mass(slha_file, gravitino_mass):

#     lines = open(slha_file).read().split('\n')
#     new_lines = []
#     for line in lines:
#         new_lines.append(line)

#         if line.strip().startswith('1000037'):
#             new_lines.append('   1000039     %.8E   # ~gravitino' % gravitino_mass)

#     with open(slha_file+'.new', 'w+') as f:
#         for line in new_lines:
#             if line:
#                 f.write(line+'\n')

#     os.system('mv %s.new %s' % (slha_file, slha_file))


# def fix_higgs_mass(input_slha_file, output_slha_file, higgs_mass=125):

#     lines = open(input_slha_file).read().split('\n')
#     new_lines = []
#     for line in lines:
#         if line.strip().startswith('25') and line.strip().endswith('# h0'):
#             new_lines.append('        25     %.8E   # h0' % higgs_mass)
#         else:
#             new_lines.append(line)

#     with open(output_slha_file, 'w+') as f:
#         for line in new_lines:
#             if line:
#                 f.write(line+'\n')




# Generate SLHA file
def generate_slha(outfile, model):

    # Create SPheno input
    write_spheno_input('input.slha', model)

    # 1. Run SPheno
    cmd = '$SPHENO_BIN input.slha'
    st = subprocess.call(cmd, shell=True)

    if st != 0:
        print('SPheno failed')
        return None

    # Rename SPheno output
    subprocess.call(f'cp SPheno.spc {outfile}', shell=True)

    return st


#
def get_slha_files(run_dir, recursive=False):

    for fname in os.listdir(run_dir):

        path = os.path.join(run_dir, fname)

        if os.path.isdir(path) and recursive:
            for rpath in get_slha_files(path):
                yield rpath

        elif os.path.isfile(path) and fname.endswith('.slha'):
            yield path


def create_scan_df(run_dir, output_file):

    datas = []

    # Loop over input spectrum files
    for infile in get_slha_files(run_dir, True):

        # if evt % 1000 == 0:
        #     print('Processing %i ...' % evt)

        ## Read spectrum file
        BLOCKS, DECAYS = None, None
        try:
            doc = pyslha.read(infile)
            BLOCKS, DECAYS = doc.blocks, doc.decays
        except:
            print(f'Error reading file {infile}. Skipping ...')
            continue

        data = {}

        ## SUSY parameters
        params = BLOCKS['EXTPAR']

        data['M1']   = params[1]
        data['M2']   = params[2]
        data['M3']   = params[3]
        data['AT']   = params[11]
        data['mu']   = params[23]
        data['tanb'] = params[25]
        data['MA']   = params[26]
        data['Msq']  = params[41]

        ## Masses
        masses = { k: abs(v) for k, v in BLOCKS['MASS'].items() }

        data['m_h0'] = masses[25]
        data['m_H0'] = masses[35]
        data['m_A0'] = masses[36]
        data['m_Hp'] = masses[37]

        data['m_gl'] = masses[1000021]
        data['m_N1'] = masses[1000022]
        data['m_N2'] = masses[1000023]
        data['m_N3'] = masses[1000025]
        data['m_N4'] = masses[1000035]
        data['m_C1'] = masses[1000024]
        data['m_C2'] = masses[1000037]
        data['m_G']  = masses[1000039]

        sorted_masses = sorted(masses.keys(), key=masses.get)
        sparticles = [i for i in sorted_masses if i>1000000]

        data['LSP']  = sparticles[0]
        data['NLSP'] = sparticles[1]

        ## N mixing matrix
        nmix = BLOCKS['NMIX']
        data['N11'] = nmix[(1,1)]
        data['N12'] = nmix[(1,2)]
        data['N13'] = nmix[(1,3)]
        data['N14'] = nmix[(1,4)]
        data['N21'] = nmix[(2,1)]
        data['N22'] = nmix[(2,2)]
        data['N23'] = nmix[(2,3)]
        data['N24'] = nmix[(2,4)]
        data['N31'] = nmix[(3,1)]
        data['N32'] = nmix[(3,2)]
        data['N33'] = nmix[(3,3)]
        data['N34'] = nmix[(3,4)]
        data['N41'] = nmix[(4,1)]
        data['N42'] = nmix[(4,2)]
        data['N43'] = nmix[(4,3)]
        data['N44'] = nmix[(4,4)]

        ## Chargino mixing matrix
        umix = BLOCKS['UMIX']
        data['C11'] = umix[(1,1)]
        data['C12'] = umix[(1,2)]
        data['C21'] = umix[(2,1)]
        data['C22'] = umix[(2,2)]


        # N1 composition
        norm_sqr = data['N11']**2 + data['N12']**2 + data['N13']**2 + data['N14']**2

        data['comp_N1_bino']      = data['N11']**2 / norm_sqr
        data['comp_N1_wino']      = data['N12']**2 / norm_sqr
        data['comp_N1_higgsino'] = (data['N13']**2+data['N14']**2) / norm_sqr

        ## Decays
        hbar = 6.582E-25      # GeV.s
        cspeed = 2.99792458E8 # m.s

        ## gluino decays
        gluino = DECAYS[1000021]

        try:
            data['length_gl'] = hbar * cspeed / gluino.totalwidth * 1000.
        except:
            data['length_gl'] = 0.

        data['BR_gl_N1qq'] = 0
        data['BR_gl_N2qq'] = 0
        data['BR_gl_N3qq'] = 0
        data['BR_gl_C1qq'] = 0

        data['BR_gl_N1g'] = 0
        data['BR_gl_N2g'] = 0
        data['BR_gl_N3g'] = 0

        data['BR_gl_C1'] = 0
        data['BR_gl_Gg'] = 0
        data['BR_gl_other'] = 0
        for dc in gluino.decays:

            pid0 = abs(dc.ids[0])
            pid1 = abs(dc.ids[1])
            pid2 = abs(dc.ids[2]) if len(dc.ids) > 2 else 0

            # ~g -> N1 X
            if pid0 == 1000022:
                if pid1 == 21:
                    data['BR_gl_N1g'] += dc.br
                elif (pid1 > 0 and pid1 < 9) and (pid2 > 0 and pid2 < 9):
                    data['BR_gl_N1qq'] += dc.br

            # ~g -> N2 X
            elif pid0 == 1000023:
                if pid1 == 21:
                    data['BR_gl_N2g'] += dc.br
                elif (pid1 > 0 and pid1 < 9) and (pid2 > 0 and pid2 < 9):
                    data['BR_gl_N2qq'] += dc.br

            # ~g -> N3 X
            elif pid0 == 1000025:
                if pid1 == 21:
                    data['BR_gl_N3g'] += dc.br
                elif (pid1 > 0 and pid1 < 9) and (pid2 > 0 and pid2 < 9):
                    data['BR_gl_N3qq'] += dc.br

            # ~g -> C1 X
            elif pid0 == 1000024:
                data['BR_gl_C1qq'] += dc.br
                data['BR_gl_C1']   += dc.br

            # ~g -> ~G g
            elif pid0 == 1000039 and pid1 == 21:
                data['BR_gl_Gg'] += dc.br

            else:
                data['BR_gl_other'] += dc.br

        data['BR_gl_N1'] = data['BR_gl_N1g'] + data['BR_gl_N1qq']
        data['BR_gl_N2'] = data['BR_gl_N2g'] + data['BR_gl_N2qq']
        data['BR_gl_N3'] = data['BR_gl_N3g'] + data['BR_gl_N3qq']

        # Fix files modified by hand
        br_total = data['BR_gl_N1'] + data['BR_gl_N2'] + data['BR_gl_N3'] + data['BR_gl_C1'] + data['BR_gl_Gg'] + data['BR_gl_other']

        if br_total < 0.99 and br_total > 0.:
            data['BR_gl_N1g']   /= br_total
            data['BR_gl_N1qq']  /= br_total
            data['BR_gl_N2g']   /= br_total
            data['BR_gl_N2qq']  /= br_total
            data['BR_gl_N3g']   /= br_total
            data['BR_gl_N3qq']  /= br_total
            data['BR_gl_C1']    /= br_total
            data['BR_gl_C1qq']  /= br_total
            data['BR_gl_Gg']    /= br_total
            data['BR_gl_other'] /= br_total

            data['BR_gl_N1'] = data['BR_gl_N1g'] + data['BR_gl_N1qq']
            data['BR_gl_N2'] = data['BR_gl_N2g'] + data['BR_gl_N2qq']
            data['BR_gl_N3'] = data['BR_gl_N3g'] + data['BR_gl_N3qq']

        # br_total = br_gl_n1 + br_gl_n2 + br_gl_n3 + br_gl_c1 + br_gl_Gg + br_gl_other

        # neutralino1 decays
        n1 = DECAYS[1000022]

        try:
            daata['length_N1'] = hbar * cspeed / n1.totalwidth * 1000.
        except:
            data['length_N1'] = 0.

        data['BR_N1_GZ'] = 0
        data['BR_N1_Gy'] = 0
        data['BR_N1_Gh'] = 0
        for dc in n1.decays:

            pid0 = abs(dc.ids[0])
            pid1 = abs(dc.ids[1])

            # N1 -> ~G Z
            if pid0 == 1000039 and pid1 == 23:
                data['BR_N1_GZ'] += dc.br

            # N1 -> ~G y
            elif pid0 == 1000039 and pid1 == 22:
                data['BR_N1_Gy'] += dc.br

            # N1 -> ~G h0
            elif pid0 == 1000039 and pid1 == 25:
                data['BR_N1_Gh'] += dc.br


        # neutralino2 decays
        n2 = DECAYS[1000023]

        try:
            data['length_N2'] = hbar * cspeed / n2.totalwidth * 1000.
        except:
            data['length_N2'] = 0.

        # neutralino2 decays
        data['BR_N2_N1'] = 0
        data['BR_N2_C1'] = 0
        data['BR_N2_GX'] = 0
        for dc in n2.decays:

            pid0 = abs(dc.ids[0])
            pid1 = abs(dc.ids[1])

            # N2 -> N1
            if pid0 == 1000022:
                data['BR_N2_N1'] += dc.br

            # N2 -> C1
            elif pid0 == 1000024:
                data['BR_N2_C1'] += dc.br

            # N2 -> ~G X
            elif pid0 == 1000039:
                data['BR_N2_GX'] += dc.br


        # neutralino3 decays
        n3 = DECAYS[1000025]

        data['BR_N3_N1'] = 0
        data['BR_N3_N2'] = 0
        data['BR_N3_C1'] = 0
        data['BR_N3_GX'] = 0
        for dc in n3.decays:

            pid0 = abs(dc.ids[0])
            pid1 = abs(dc.ids[1])

            # N3 -> N1
            if pid0 == 1000022:
                data['BR_N3_N1'] += dc.br

            # N3 -> N2
            elif pid0 == 1000023:
                data['BR_N3_N2'] += dc.br

            # N3 -> C1
            elif pid0 == 1000024:
                data['BR_N3_C1'] += dc.br

            # N3 -> ~G X
            elif pid0 == 1000039:
                data['BR_N3_GX'] += dc.br

        # chargino1 decays
        c1 = DECAYS[1000024]

        data['BR_C1_N1'] = 0
        data['BR_C1_N1qq'] = 0
        data['BR_C1_N1lnu'] = 0
        data['BR_C1_GW'] = 0
        for dc in c1.decays:

            pid0 = abs(dc.ids[0])
            pid1 = abs(dc.ids[1])

            # C1 -> N1
            if pid0 == 1000022:
                data['BR_C1_N1'] += dc.br

                if pid1 < 6:
                    data['BR_C1_N1qq'] += dc.br
                else:
                    data['BR_C1_N1lnu'] += dc.br

            # C1 -> ~G W
            elif pid0 == 1000039 and pid1 == 24:
                data['BR_C1_GW'] += dc.br


        datas.append(data)


    # Save Dataframe
    df = pd.DataFrame.from_dict(datas) #, columns=columns)
    df.to_csv(output_file, index=False)

GGM Scan
========

Download and install all the needed tools (SPheno and python packages)

```
./install.sh

or

./install_osx.sh
```

Setup

```
source setup.sh
```

Alternatively you can use the docker image:

```
docker run -it --rm gitlab-registry.cern.ch/falonso/ggm-scan:latest
```

Usage:

```
ggmscan.py -c CONFIGFILE [-o OUTPUTDIR] [-v] --scan --df
```

The configfile must have the following lists defining the phase space to scan:

```
s_mu, s_M1, s_M2, s_M3, s_AT, s_Gmass, s_Msq, s_tanb
```

It can also contain a filter function (based on parameters or the slha parameters). The functions must return True to ignore the point

```
def filter_par_fn(M1, M2, M3, mu, tanb, Msq, AT, Gmass):
    ...

def filter_slha_fn(slha_path):
    ...
```


## Variables in the output dataframe

(Check details in "create_scan_df" function in utils.py)

- SUSY parameters
    - M1: bino mass term
    - M2: wino mass term
    - M3: gluino mass term
    - Msq: squarks mass term
    - AT: trilinear terms
    - mu: higgs mass term
    - tanb: $\tan(\beta)$ ratio between Higgs VEVs

- Masses
    - m_h0: CP-even neutral light Higgs
    - m_H0: CP-even neutral heavy Higgs
    - m_A0: CP-odd neutral Higgs
    - m_Hp: charged Higgs
    - m_gl: gluino
    - m_N1: neutralino 1
    - m_N2: neutralino 2
    - m_N3: neutralino 3
    - m_N4: neutralino 4
    - m_C1: chargino 1
    - m_C2: chargino 2
    - m_G: Gravitino

- LSP/NLSP
    - LSP: PDG id of the LSP
    - NLSP: PDG id of the NLSP

- Neutralino/Charginos mixing matrix
    - NXY (X, Y = 1, 2, 3, 4)
    - CXY (X,Y = 1, 2)

- Neutralino1 composition computed as

    - comp_N1_bino: $$N11^2 / N^2$$
    - comp_N1_wino: $$N12^2 / N^2$$
    - comp_N1_higgsino: $$(N13^2 + N14^2) / N^2$$

where $N^2 = \sum_i N1i^2$.

- Decays (lengths and BRs)

    - Gluino
        - length_gl
        - BR_gl_N1qq $$\text{BR}(\tilde{g}\to N_1 q\bar{q})$$

        - BR_gl_N2qq: $$\text{BR}(\tilde{g}\to N_2 q\bar{q})$$
        - BR_gl_N3qq: $$\text{BR}(\tilde{g}\to N_3 q\bar{q})$$
        - BR_gl_C1qq: $$\text{BR}(\tilde{g}\to C_1 q\bar{q})$$
        - BR_gl_N1g:  $$\text{BR}(\tilde{g}\to N_1 g)$$
        - BR_gl_N2g:  $$\text{BR}(\tilde{g}\to N_2 g)$$
        - BR_gl_N3g:  $$\text{BR}(\tilde{g}\to N_3 g)$$
        - BR_gl_N1:   $$\text{BR}(\tilde{g}\to N_1 X)$$
        - BR_gl_N2:   $$\text{BR}(\tilde{g}\to N_2 X)$$
        - BR_gl_N3:   $$\text{BR}(\tilde{g}\to N_3 X)$$
        - BR_gl_C1:   $$\text{BR}(\tilde{g}\to C_1 X)$$
        - BR_gl_Gg:   $$\text{BR}(\tilde{g}\to \tilde{G} g)$$
        - BR_gl_other:$$\text{BR}(\tilde{g}\to \text{other})$$

    - Neutralino1
        - length_N1
        - BR_N1_Gy: $$\text{BR}(N_1 \to \gamma\tilde{G})$$
        - BR_N1_GZ: $$\text{BR}(N_1 \to Z\tilde{G})$$
        - BR_N1_Gh: $$\text{BR}(N_1 \to h\tilde{G})$$

    - Neutralino2
        - length_N2
        - BR_N2_n1: $$\text{BR}(N_2 \to N_1 X)$$
        - BR_N2_c1: $$\text{BR}(N_2 \to C_1 X)$$
        - BR_N2_GX: $$\text{BR}(N_2 \to X\tilde{G})$$

    - Neutralino3
        - BR_N3_N1: $$\text{BR}(N_3 \to N_1 X)$$
        - BR_N3_N2: $$\text{BR}(N_3 \to N_2 X)$$
        - BR_N3_C1: $$\text{BR}(N_3 \to N_3 X)$$
        - BR_N3_GX: $$\text{BR}(N_3 \to X\tilde{G})$$

    - Chargino1
        - BR_C1_N1:    $$\text{BR}(C_1 \to N_1 X)$$
        - BR_C1_N1qq:  $$\text{BR}(C_1 \to N_1 q\bar{q})$$
        - BR_C1_N1lnu: $$\text{BR}(C_1 \to N_1 \ell\nu)$$
        - BR_C1_GW:    $$\text{BR}(C_1 \to W\tilde{G})$$

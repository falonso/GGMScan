#! /usr/bin/env python

import os
import sys
import json
import datetime
import argparse
from progressbar import ProgressBar

from utils import (
    read_scan_config_file,
    create_run_directory,
    clean_run_directory,
    generate_slha,
    create_scan_df
)

is_py3 = (sys.version_info > (3, 0))

def get_output_slha_name(model):
    outfile = 'scan_M1_{M1}_M2_{M2}_M3_{M3}_mu_{mu}_MA_{MA}_tanb_{tanb}_Msq_{Msq}_AT_{AT}_Gmass_{Gmass}'.format(**model)
    outfile = outfile.replace('.', 'p').replace('-', 'm')
    outfile += '.slha'
    return outfile

def do_scan(run_dir, models, debug):

    print(f'\nStarting scan in {run_dir}')

    prev_dir = os.getcwd()
    os.chdir(run_dir)

    try:
        bar = ProgressBar(len(models))

        rm_files = 0

        progress = 0 #len(done_files) + 1
        for model in models:

            progress += 1

            # Output slha file name
            outfile = get_output_slha_name(model)

            # if outfile in done_files:
            #     continue

            if debug:
                print(outfile)

            st = generate_slha(outfile, model)

            ## Check slha filter
            # if filter_fn_slha(outfile):
            #     rm_files += 1
            #     os.system('rm %s' % outfile)

            if not debug:
                bar.print_bar(progress)

        # end of loops
        print()
        print('Done.')
        if rm_files > 0:
            print(f'{rm_files} files removed due to the slha filter')

    except KeyboardInterrupt:
        print('Scan interrupted')
        pass


    os.chdir(prev_dir)

    # Clean directory
    clean_run_directory(run_dir)


def main():

    parser = argparse.ArgumentParser(description='')

    parser.add_argument('-c', '--config', dest='configfile', help='Scan configuration file')
    parser.add_argument('-m', '--models', dest='modelsfile', help='Json file with models ')

    parser.add_argument('-o', dest='outputdir', help='Output directory')

    parser.add_argument('--scan', action='store_true', help='Do parameter scan')
    parser.add_argument('--df', action='store_true', help='Create dataframe from slha files in output directory')

    parser.add_argument('--debug', action='store_true', help='Debug output')

    #global args
    args = parser.parse_args()

    if not args.scan and not args.df:
        parser.print_usage()
        sys.exit(1)

    if args.configfile is None and args.modelsfile is None:
        parser.print_usage()
        sys.exit(1)

    # Scan configuration
    if args.configfile is not None:
        models = read_scan_config_file(args.configfile)

    elif args.modelsfile is not None:

        with open(args.modelsfile) as f:
            models = json.load(f)


    # Count number of models
    n_models = len(models)
    print(f'Number of models: {n_models}')


    # Run directory
    if args.outputdir is None:
        t = datetime.datetime.now()
        datestr = '%s-%s-%s_%s.%s.%s' % (t.year, t.month, t.day, t.hour, t.minute, t.second)
        run_dir = f'{os.getcwd()}/ggm_scan_run_{datestr}'
    else:
        run_dir = args.outputdir

    if not os.path.exists(run_dir):
        create_run_directory(run_dir)

    # Check scan status
    # slha_files = glob.glob(f'{run_dir}/scan_m1*.slha')
    # if len(slha_files) > 0:
    #     done_files = [ f for f in slha_files if os.path.isfile(f) ]
    #     #done_files.sort(key=lambda x: os.path.getmtime(x))
    #     done_files = done_files[:-1] # FIX: check for errors in done_files
    #     print('Found %i slha files already done. Continue with the remaining points...' % len(done_files))
    # else:
    #     done_files = []


    if args.scan:
        do_scan(run_dir, models, args.debug)


    # Create scan dataframe
    if args.df:
        output_file = f'{run_dir}/scan.csv'
        print(f'Creating dataframe in {output_file} ...')
        create_scan_df(run_dir, output_file)
        print('Done.')


if __name__ == '__main__':
    main()

#! /usr/bin/env python
#
import os
import sys
import argparse
import json
import numpy as np
import htcondor

from utils import read_scan_config_file

# Run script template
template_run_script = """#!/bin/bash

models_name=$1

ls

echo $PATH

source /setup_ggmscan.sh

ggmscan.py --models ${models_name}.json -o output --scan

tar -cvzf output_${models_name}.tar.gz output/*.slha

"""


def main():

    parser = argparse.ArgumentParser(description='')

    parser.add_argument('-c', dest='configfile', required=True, help='Scan configuration file')
    parser.add_argument('--split', type=int, required=True, help='Split in N jobs')
    parser.add_argument('-o', dest='outputdir', required=True, help='Output directory')

    args = parser.parse_args()

    output_dir = args.outputdir
    if not os.path.exists(output_dir):
        os.system(f'mkdir -p {output_dir}')



    # Models
    models = read_scan_config_file(args.configfile)

    # Filter models with filter_fn_par
    # for model in models[:]:
    #     if filter_fn_par_copy(model):
    #         models.remove(model)

    n_models = len(models)
    print(f'Number of models: {n_models}')


    # Split models
    n_jobs = args.split
    split_models = np.array_split(models, n_jobs)

    # Save splitted models
    for i, ml in enumerate(split_models):
        with open(f"{output_dir}/models_{i}.json", "w") as f:
            json.dump(ml.tolist(), f)


    # Run script
    script_path = f'{output_dir}/run_scan.sh'
    print(f'# Preparing run script: {script_path}')
    with open(script_path, 'w') as f:
        f.write(template_run_script)

    os.chmod(script_path, 0o755)


    # Prepare and send jobs
    os.chdir(output_dir)

    job = htcondor.Submit({
        "universe": "container",
        "container_image": "/mnt/R5/images/ggm-scan-latest.sif",

        "executable": "run_scan.sh",
        "arguments": "models_$(Process)",
        "output": "job_$(Cluster)_$(Process).out",
        "error":  "job_$(Cluster)_$(Process).err",
        "log":    "job_$(Cluster)_$(Process).log",

        "should_transfer_files": "YES",
        "transfer_input_files": "models_$(Process).json",
        "transfer_output_files": "output_models_$(Process).tar.gz",
        "when_to_transfer_output": "ON_EXIT",
    })

    print(job)

    schedd = htcondor.Schedd()
    submit_result = schedd.submit(job, count=n_jobs)

    print(submit_result.cluster())



if __name__ == '__main__':
    main()

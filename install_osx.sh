#! /usr/bin/bash

SRC_DIR=$PWD
INSTALL_DIR=$PWD/tools_osx

mkdir $INSTALL_DIR
cd $INSTALL_DIR

# SPheno
curl -0 "https://spheno.hepforge.org/downloads/?f=SPheno-4.0.5.tar.gz" -o SPheno-4.0.5.tar.gz
tar -xvzf SPheno-4.0.5.tar.gz
cd SPheno-4.0.5
sed -i '' 's|ifort|gfortran|g' Makefile
make

# Python packages
cd $INSTALL_DIR
python -m venv --system-site-packages ggm-scan-venv
source ggm-scan-venv/bin/activate
python -m pip install --no-cache-dir --upgrade --ignore-installed -r $SRC_DIR/requirements.txt
deactivate


cd $SRC_DIR
